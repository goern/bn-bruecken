# Changelog

## [0.12.0] - goern - 2018-Feb-13

### Added

Now there is a retweeter component, it will listen on a Twitter Stream, and for each received status update it will query a machine learning based webservice to predict if the status shall be retweeted.

## [0.11.0] - goern - 2018-Feb-10

### Added

Temperature Warnings use one of four different Texts.

Now we tweet speed info only of difference is great enough (>44km/h)

## [0.7.0] - goern - 2017-Nov-13

### Added

`config.json` is now read and can be used to set probabilities to tweet some info and to set hours to actively tweet.

### Changes

Now using opendata-bonn nodejs module.

## [0.6.0] - goern - 2017-Nov-07

### Added

At some random interval weather and speed information is send out to twitter.

### Changed

Job names in `package.json` and `Procfile` have changed.

## [0.4.0] and [0.4.1] - goern - 2017-Nov-02

### Added

Data gathered are now stored to a MongoDB for future use.

### Changed

`streck_id` was in wrong/reverse order.

### Removed

Nothing.
