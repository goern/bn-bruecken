/*
 *   bn-bruecken
 *   Copyright (C) 2017,2018  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const pjson = require('./package.json');
const version = pjson.version;

const moment = require('moment-timezone');
const mongoose = require('mongoose');
const redis = require('redis');

// This is what the bot needs
const opendata = require('opendata-bonn');
const helper = require('./lib/helper.js');
const Bot = require('./bot');
const model = require('./lib/schema.js');

// let's say who we are and read our config
console.log('bn-bruecke v' + version);
const config = helper.getConfig('config.json');
console.log(config);

const antwortenTemperatur = [
    'Es ist kalt! {value} Grad... Es könnte glatt sein!',
    '... fahrt vorsichtig, es ist nur {value} Grad.',
    'laaangsam, es ist {value} Grad kalt!',
    'Leute, passt auf! es ist kalt... {value} Grad',
];

let redisClient = redis.createClient(process.env.REDIS_URL);
redisClient.unref(); // seeAlso https://github.com/NodeRedis/node_redis#clientunref

redisClient.on('error', function(err) {
    console.log('Error ' + err);
});

redisClient.on('connect', function() {
    console.log('Redis connected');
});

moment.locale('de');
moment()
    .tz('Europe/Berlin')
    .format();

mongoose.connect(process.env.MONGODB_URI, {
    useMongoClient: true,
});
mongoose.Promise = global.Promise; // Use native promises
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'database connection error:'));

db.once('open', function() {
    console.log('MongoDB connected');

    let bot = new Bot({
        consumer_key: process.env.BOT_CONSUMER_KEY,
        consumer_secret: process.env.BOT_CONSUMER_SECRET,
        access_token: process.env.BOT_ACCESS_TOKEN,
        access_token_secret: process.env.BOT_ACCESS_TOKEN_SECRET,
    });

    bot.twit.get('followers/ids', function(err, reply) {
        if (err) return helper.handleError(err);
        console.log('Followers:' + reply.ids.length.toString());
    });

    let rand = Math.random();
    console.log('Random Value is %s', rand);

    // lets broadcast my version...
    if (rand <= config.probabilityOfVersionTweet) {
        if (process.env.DEPLOYMENT_MODE != 'LOCAL') {
            bot.tweet(
                'ich laufe mit v' +
                version +
                ' von bn-bruecken, mehr unter http://lnks.li/bn-bruecken',
                function(err, reply) {
                    if (err) return helper.handleError(err);
                    else {
                        console.log('Tweet: ' + (reply ? reply.text : reply));
                    }
                }
            );
        }
    }

    // lets thet the data and tweet it, if it changes
    opendata
        .GetAktuelleStraßenverkehrslage(
            opendata.FriedrichEbertBrückeBeideRichtungen
        )
        .then(function(geojson) {
            let status_to_beuel = 'unbekannt';
            let status_to_bonn = 'unbekannt';
            let auswertezeit = moment();

            let status = new model.TrafficStatus({
                object: 'fe_bruecke',
            });

            geojson.features.forEach(function(element) {
                if (
                    element.properties.strecke_id ==
                    opendata.FriedrichEbertBrückeBonnNachBeuel
                ) {
                    status_to_beuel = element.properties.verkehrsstatus;
                    auswertezeit = moment.tz(
                        element.properties.auswertezeit,
                        'YYYY-MM-DDThh:mm:ss',
                        'Europe/Berlin'
                    );

                    let statusToBeuel = new model.DirectionalStatus({
                        description: 'nach Beuel',
                        text: status_to_beuel,
                        speed: element.properties.geschwindigkeit,
                        strecke: element.properties.strecke_id,
                    });

                    status.directionA = statusToBeuel;

                    redisClient.set(
                        'fe_bruecke.toBeuel.speed',
                        statusToBeuel.speed,
                        function(err, reply) {
                            if (err != null) {
                                console.log(err);
                            } else {
                                console.log(reply);
                            }
                        }
                    );

                    if (process.env.DEPLOYMENT_MODE != 'LOCAL') {
                        directionASavePromise = statusToBeuel.save();
                    }

                    status.updated_at = auswertezeit;
                }
                if (
                    element.properties.strecke_id ==
                    opendata.FriedrichEbertBrückeBeuelNachBonn
                ) {
                    status_to_bonn = element.properties.verkehrsstatus;
                    auswertezeit = moment.tz(
                        element.properties.auswertezeit,
                        'YYYY-MM-DDThh:mm:ss',
                        'Europe/Berlin'
                    );

                    let statusToBonn = new model.DirectionalStatus({
                        description: 'nach Bonn',
                        text: status_to_bonn,
                        speed: element.properties.geschwindigkeit,
                        strecke: element.properties.strecke_id,
                    });

                    if (process.env.DEPLOYMENT_MODE != 'LOCAL') {
                        directionBSavePromise = statusToBonn.save();
                    }

                    status.directionB = statusToBonn;
                    status.updated_at = auswertezeit;

                    redisClient.set(
                        'fe_bruecke.toBonn.speed',
                        statusToBonn.speed
                    );
                    redisClient.set(
                        'fe_bruecke.updated_at',
                        status.updated_at.fromNow()
                    );
                }
            }, this);

            let tweetText = '';
            if (status_to_beuel == status_to_bonn) {
                tweetText =
                    'In beide Fahrtrichtungen ' +
                    status_to_beuel +
                    ' #A565 (Daten von ' +
                    auswertezeit.fromNow() +
                    ')';
            } else {
                tweetText =
                    '#A565 in Richtung Beuel ' +
                    status_to_beuel +
                    ' und #A565 nach Bonn ' +
                    status_to_bonn +
                    ' (Daten von ' +
                    auswertezeit.fromNow() +
                    ')';
            }

            if (tweetText != '') {
                tweetText.replace('Stau', '#Stau');
                tweetText = tweetText.replace('/^(.{100}[^s]*).*/', '$1');

                sendTweet(bot, tweetText, false);
            }

            // if we are in the clouds... save!
            if (process.env.DEPLOYMENT_MODE != 'LOCAL') {
                let statusSavePromise = status.save();

                statusSavePromise.then(function(doc) {
                    db.close();
                });
            } else {
                console.log('closing MongoDB...');
                db.close();
            }
        })
        .catch(function(err) {
            console.log(err);
        });

    // lets tweet about speed on the bridge
    if (
        process.env.DEPLOYMENT_MODE == 'LOCAL' ||
        rand <= config.probabilityOfSpeedTweet
    ) {
        redisClient.mget(
            [
                'fe_bruecke.updated_at',
                'fe_bruecke.toBeuel.speed',
                'fe_bruecke.toBonn.speed',
            ],
            function(err, reply) {
                if (reply != null) {
                    let tweetText =
                        '' +
                        reply[0] +
                        '... Geschwindigkeit nach Beuel: ca. ' +
                        reply[1] +
                        ' km/h und Richtung Bonn: ' +
                        reply[2] +
                        ' km/h';

                    if (reply[1] < 25 || reply[2] < 25) {
                        tweetText += ' #Stau';
                    } else if (reply[1] < 45 || reply[2] < 45) {
                        tweetText += ' #Staugefahr';
                    }

                    // nur tweeten wenn der Unterschied groß genug ist
                    if (Math.abs(reply[1] - reply[2]) > 44) {
                        sendTweet(bot, tweetText, false);
                    }
                }
            }
        );
    }

    // lets send out some weather info...
    if (
        process.env.DEPLOYMENT_MODE == 'LOCAL' ||
        rand <= config.probabilityOfWeatherTweet
    ) {
        redisClient.mget(
            [
                'fe_bruecke.weather.temperature',
                'fe_bruecke.weather.summary',
                'fe_bruecke.weather.icon',
                'fe_bruecke.toBeuel.speed',
                'fe_bruecke.toBonn.speed',
            ],
            function(err, reply) {
                // if temperature is < 0.1 C we will tweet some warning
                if (reply[0] < 0.1) {
                    const answer =
                        antwortenTemperatur[
                            Math.floor(
                                Math.random() * antwortenTemperatur.length
                            )
                        ];

                    const answerText = format(answer, {
                        value: reply[0],
                    });

                    sendTweet(bot, answerText, false);
                } else {
                    tweetText = reply[1] + ' momentan... ';

                    switch (reply[2]) {
                        case 'partly-cloudy-night':
                        case 'partly-cloudy':
                            tweetText += '⛅';
                            break;
                        case 'clear-day':
                            tweetText += '☀️';
                            break;
                        case 'clear-night':
                            tweetText += '✨';
                            break;
                        default:
                            break;
                    }

                    sendTweet(bot, tweetText, false);
                }
            }
        );
    }
}); // db.on('connect')

/**
 * sendTweet() will use a bot instance and send out a text.
 *
 * @param {any} bot
 * @param {any} tweetText
 * @param {bool} forceTweet
 */
function sendTweet(bot, tweetText, forceTweet) {
    tweetText.replace(/�/, 'ö'); // tweetText
    if (process.env.DEPLOYMENT_MODE != 'LOCAL') {
        let jetzt = new Date();
        if (
            (jetzt.getUTCHours() >= config.startService &&
                jetzt.getUTCHours() <= config.endService) ||
            forceTweet
        ) {
            // lets send out tweets between t1 and t2
            bot.tweet(tweetText, function(err, reply) {
                if (err) return helper.handleError(err);
                console.log('Tweet: ' + (reply ? reply.text : reply));
            });
        } else {
            // we are off hours
            console.log('running OFF HOURS, tweet: %s', tweetText);
        }
    } else {
        console.log('running in LOCAL mode, tweet: %s', tweetText);
    }
}