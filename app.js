/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const version = '0.5.0-dev';

import express from 'express';

console.log('bn-bruecke v' + version);

let app = express();

app.set('port', (process.env.PORT || 5000));
app.use(express.static('public'));

app.get('/', function(req, res) {
    res.redirect('https://twitter.com/fe_bruecke');
});

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});
