#!/usr/bin/env python
#   bn-bruecken
#   Copyright (C) 2018  Christoph Görn
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""app.py: This is a bot retweeting stuff, the decision to
retweet or not is based on a machine learning model aka. AI!"""

__version__ = '0.12.0'

import os
import logging

import tweepy
import requests


DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))
PREDICT_SERVICE_HOST = 'http://retweetmodel-bn-bruecken-retweetmodel.e8ca.engint.openshiftapps.com'

def is_good_for_retweet(text: str) -> bool:
    r = requests.post(PREDICT_SERVICE_HOST+'/predict', 
        json={"text": text})

    if r.status_code == 200:
        result = r.json()
        logger.debug(result)

        if result['retweet'] == True:
            return True

    return False


class FilteringStreamListener(tweepy.StreamListener):
    def on_status(self, status):
        global api, logger

        if (status._json['truncated']):
            retweet = is_good_for_retweet(status._json['extended_tweet']['full_text'])
        else:
            retweet = is_good_for_retweet(status._json['text'])

        logger.debug(retweet)

        if retweet:
            api.retweet(status.ID)

    def on_error(self, status_code):
        if status_code == 420:
            return False


CONSUMER_KEY = os.environ.get('TWITTER_CONSUMER_KEY')
CONSUMER_SECRET = os.environ.get('TWITTER_CONSUMER_SECRET')
ACCESS_TOKEN = os.environ.get('TWITTER_ACCESS_TOKEN')
ACCESS_TOKEN_SECRET = os.environ.get('TWITTER_ACCESS_TOKEN_SECRET')

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

api = tweepy.API(auth)

if __name__ == '__main__':
    """The main function
    This will launch the main stream processor class.
    """

    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    else:
        logging.basicConfig(level=logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info('initializing...')

    is_good_for_retweet('Hallo A565')

    listener = FilteringStreamListener()
    stream = tweepy.Stream(auth=api.auth, listener=listener, tweet_mode='extended')

    # this is WDR_Verkehr, DB_Bahn, Polizei_NRW_BN, Strassen.NRW
    stream.filter(follow=['1224885026', '39999078', '2389161066', '386445538'])
