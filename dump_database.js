/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

const version = '0.3.0';

var mongoose = require('mongoose');
var model = require('./lib/schema.js');

mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
mongoose.Promise = global.Promise; // Use native promises
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'database connection error:'));

db.once('open', function () {
    console.log('database connected');

    var status = new model.TrafficStatus({ object: 'test' });
    var promise = status.save();

    promise.then(function (doc) {
        console.log(doc);

        var query = model.TrafficStatus.find();

        query.then(function (doc) {
            console.log(doc);

            db.close();
        });
    });

});



