/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

const pjson = require("./package.json");
const version = pjson.version;

var moment = require('moment');
var moment = require('moment-timezone');
var redis = require('redis');
const DarkSky = require('dark-sky')

const darksky = new DarkSky(process.env.DARK_SKY_SECRET_KEY)

console.log('bn-bruecke v' + version);

moment.locale('de');
moment().tz("Europe/Berlin").format();

var redisClient = redis.createClient(process.env.REDIS_URL);
redisClient.unref() // seeAlso https://github.com/NodeRedis/node_redis#clientunref

redisClient.on("error", function(err) {
    console.log("Error " + err);
});

redisClient.on("connect", function() {
    console.log('Redis connected');

    darksky
        .options({
            latitude: 50.7345,
            longitude: 7.10054,
            time: moment(),
            language: 'de',
            exclude: ['minutely', 'hourly', 'daily'],
            units: 'ca'
        })
        .get()
        .then(function(forecast) {
            console.debug(forecast);

            redisClient.mset('fe_bruecke.weather.temperature', forecast.currently.temperature,
                'fe_bruecke.weather.summary', forecast.currently.summary,
                'fe_bruecke.weather.icon', forecast.currently.icon,
                function(err, reply) {
                    if (err != null) {
                        console.log(err);
                    } else {
                        console.debug(reply);
                    }
                });
        });

}); // redisClient.on('connect')