/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const version = '0.5.0-dev';

let Twit = require('twit');

let Bot = module.exports = function(config) {
  this.twit = new Twit({
    consumer_key: process.env.BOT_CONSUMER_KEY,
    consumer_secret: process.env.BOT_CONSUMER_SECRET,
    access_token: process.env.BOT_ACCESS_TOKEN,
    access_token_secret: process.env.BOT_ACCESS_TOKEN_SECRET,
  });
};

//
//  post a tweet
//
Bot.prototype.tweet = function(status, callback) {
  if (typeof status !== 'string') {
    return callback(new Error('tweet must be of type String'));
  } else if (status.length > 140) {
    return callback(new Error('tweet is too long: ' + status.length));
  }
  this.twit.post('statuses/update', {status: status}, callback);
};

// choose a random tweet and follow that user
Bot.prototype.searchFollow = function(params, callback) {
  let self = this;

  self.twit.get('search/tweets', params, function(err, reply) {
    if (err) return callback(err);

    let tweets = reply.statuses;
    let rTweet = randIndex(tweets);
    if (typeof rTweet != 'undefined') {
      let target = rTweet.user.id_str;

      self.twit.post('friendships/create', {id: target}, callback);
    }
  });
};

//
// retweet
//
Bot.prototype.retweet = function(params, callback) {
  let self = this;

  self.twit.get('search/tweets', params, function(err, reply) {
    if (err) return callback(err);

    let tweets = reply.statuses;
    let randomTweet = randIndex(tweets);
    if (typeof randomTweet != 'undefined') {
      self.twit.post('statuses/retweet/:id', {id: randomTweet.id_str},
        callback);
    }
  });
};

//
// favorite a tweet
//
Bot.prototype.favorite = function(params, callback) {
  let self = this;

  self.twit.get('search/tweets', params, function(err, reply) {
    if (err) return callback(err);

    let tweets = reply.statuses;
    let randomTweet = randIndex(tweets);
    if (typeof randomTweet != 'undefined') {
      self.twit.post('favorites/create', {id: randomTweet.id_str},
        callback);
    }
  });
};

//
//  choose a random friend of one of your followers, and follow that user
//
Bot.prototype.mingle = function(callback) {
  let self = this;

  this.twit.get('followers/ids', function(err, reply) {
    if (err) {
      return callback(err);
    }

    let followers = reply.ids;
    let randFollower = randIndex(followers);

    self.twit.get('friends/ids', {user_id: randFollower}, function(err, reply) {
      if (err) {
        return callback(err);
      }

      let friends = reply.ids;
      let target = randIndex(friends);

      self.twit.post('friendships/create', {id: target}, callback);
    });
  });
};

//
//  prune your followers list; unfollow a friend that hasn't followed you back
//
Bot.prototype.prune = function(callback) {
  let self = this;

  this.twit.get('followers/ids', function(err, reply) {
    if (err) return callback(err);

    let followers = reply.ids;

    self.twit.get('friends/ids', function(err, reply) {
      if (err) return callback(err);

      let friends = reply.ids;
      let pruned = false;

      while (!pruned) {
        let target = randIndex(friends);

        if (!~followers.indexOf(target)) {
          pruned = true;
          self.twit.post('friendships/destroy', {id: target}, callback);
        }
      }
    });
  });
};

/**
 * Gives back a random element from the Array
 *
 * @param {any} arr
 * @returns
 */
function randIndex(arr) {
  let index = Math.floor(arr.length * Math.random());
  return arr[index];
};
