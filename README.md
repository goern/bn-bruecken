# BN-Bruecken

This is a twitter bot running [Nord-Brücke](https://twitter.com/fe_bruecke) using [OpenData](https://opendata.bonn.de/) provided by [Bundesstadt](https://twitter.com/BundesstadtBonn).

Source code is at https://gitlab.com/goern/bn-bruecken

[![pipeline status](https://gitlab.com/goern/bn-bruecken/badges/master/pipeline.svg)](https://gitlab.com/goern/bn-bruecken/commits/master)

# Setup

Set the following environtment variables:

```bash
BOT_CONSUMER_KEY
BOT_CONSUMER_SECRET
BOT_ACCESS_TOKEN
BOT_ACCESS_TOKEN_SECRET
DARK_SKY_SECRET_KEY
MONGODB_URI
REDIS_URL
```

and run `npm start`.

There is a `bot_job.js` which is suitable for being run as a cron job...

## Building a Docker container image

To build a docker container image from this Node.js application you need to run `docker build --rm --tag <username>/bn-bruecken`. This will use [Docker Inc. official Node](https://hub.docker.com/_/node/) container image version 6.

## Running as a container

The container image is meant as a cronjob like application, so you will have to take care to run it periodically. To run it use `docker run --rm --env-file=.env <username>/bn-bruecken`. If you want to run the container image I prepared and released on gitlab, use `docker run --rm registry.gitlab.com/goern/bn-bruecken:v0.4.0`.

Dont forget to set all the required ENV via `-e` ... or use `--env-file` and provide a `.env` file.

# Datenquelle

Datenquelle: Bundesstadt Bonn, Amt 66, https://opendata.bonn.de

## Zusätzliche Informationen

* 'strecke_id' 35 scheint die Kennedy-Brücke in Richtung Bonn zu sein.
* 'strecke_id' 36 scheint die Kennedy-Brücke in Richtung Beuel zu sein.
* 'strecke_id' 69 scheint die Friedrich-Ebert-Brücke in Richtung Bonn zu sein.
* 'strecke_id' 70 scheint die Friedrich-Ebert-Brücke in Richtung Beuel zu sein.

# Copyright

bn-bruecken
Copyright (C) 2017,2018 Christoph Görn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
