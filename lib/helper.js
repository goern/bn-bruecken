/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const pjson = require("../package.json");
const version = pjson.version;

module.exports = {
    version: version,

    handleError: function(err) {
        console.error('response status:', err.statusCode);
        console.error('data:', err);
    },

    datestring: function() {
        const d = new Date(Date.now() - 5 * 60 * 60 * 1000);

        return d.getUTCFullYear() + '-' +
            (d.getUTCMonth() + 1) + '-' +
            d.getDate();
    },

    getConfig: function(configFile) {
        const fs = require('fs');
        var config = null;

        try {
            config = JSON.parse(fs.readFileSync(configFile, 'utf8'));
        } catch (err) {
            console.log(err);
        }

        return config;
    }
};