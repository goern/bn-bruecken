/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

const pjson = require("../package.json");
const version = pjson.version;

var mongoose = require('mongoose');

var statusSchema = mongoose.Schema({
    description: String,
    text: String,
    strecke: Number,
    speed: Number,
    unit: String
});

var trafficStatusSchema = mongoose.Schema({
    object: String,
    directionA: { type: mongoose.Schema.Types.ObjectId, ref: 'DirectionalStatus' },
    directionB: { type: mongoose.Schema.Types.ObjectId, ref: 'DirectionalStatus' },
    update_at: Date
});

// lets create the models we need
var TrafficStatus = mongoose.model('TrafficStatus', trafficStatusSchema);
var DirectionalStatus = mongoose.model('DirectionalStatus', statusSchema);

// export all the things
module.exports = {
    version: version,

    statusSchema: statusSchema,
    trafficStatusSchema: trafficStatusSchema,
    TrafficStatus: TrafficStatus,
    DirectionalStatus: DirectionalStatus
};