/*
 *   bn-bruecken
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const assert = require("assert");
const expect = require('chai').expect;
const helper = require('../lib/helper');
var chai = require("chai"),
    should = chai.should();

describe('Application', () => {
    describe('GetConfig', () => {
        it('should return null if the config file does not exist', () => {
            const config = helper.getConfig('./test/fixtures/file_not_found.json');
            assert.equal(config, null);
        });
    });
});